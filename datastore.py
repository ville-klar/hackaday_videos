import os
import redis
import json 

redisClient = redis.from_url(os.environ.get("REDIS_URL"))

def readRedis():
    serialized = redisClient.get('links')
    links = json.loads(serialized)
    return links


def updateRedis(links):
    serialized = json.dumps(links)
    redisClient.set('links', serialized)

