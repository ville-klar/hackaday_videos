import feedparser
import requests
from bs4 import BeautifulSoup
import re

def extractVideoLinks():
    url = "https://hackaday.com/feed/"
    NewsFeed = feedparser.parse(url)
    out = []
    for entry in NewsFeed.entries:
        # entry = NewsFeed.entries[0]
        content = requests.get(entry['link']).content
        soup = BeautifulSoup(content,features="html.parser")
        result = soup.find_all(attrs={'class': 'embed-youtube'})
        if (len(result) == 0):
            # print("No video found")
            pass
        else:
            title = entry['title']
            description = entry['description']
            video_url = re.search("(?P<url>https?://[^\s]+)", str(result)).group("url") # Only returns the first embedded video
            if ("videoseries" in video_url):
                res = {"title":title, "link":video_url, "description":description} 
            else:
                video_url = re.search(".+?(?=\\?version)", video_url)
                res = {"title":title, "link":video_url.group(), "description":description} 
            out.append(res)
    return(out)
 
  
if __name__ == "__main__":
    links = extractVideoLinks()                 # 
    print(links)
