# Hackaday videos to RSS feed

This is an example on how to combine web scraping and RSS feed generation in Python.


**app.py**: Flask app that retrieves the latest video list (dict) from redis and serves it as RSS feed (xml) 

**clock.py**: Scheduled function (runs every 8 hours) that scrapes hackaday RSS feed for youtube videos and updates the redis list

**datastore.py**: Contains functions for updating and reading the Redis datastore

**scrape.py**: Contains the function for scraping hackaday for posts with embedded videos
