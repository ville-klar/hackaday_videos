import PyRSS2Gen
from flask import Flask
import datastore

app = Flask(__name__)

def outputXML(links):
    rss_items = []
    for entry in links:
        rss_items.append(PyRSS2Gen.RSSItem(title = entry['title'],link = entry['link'], description = entry['description']))
        rss = PyRSS2Gen.RSS2(
            title = "Hackaday Videos",
            link = "",
            description = "Videos from Hackaday posts",
            items = rss_items)
    return rss.to_xml()

@app.route('/')
def root():
    links = datastore.readRedis()
    return outputXML(links)

if __name__ == "__main__":
    app.run(threaded=True,port=5000)
