import scrape
import datastore 
from apscheduler.schedulers.blocking import BlockingScheduler

sched = BlockingScheduler()

@sched.scheduled_job('interval', hours=2)
def timed_job():
    print("Updating the redis...")
    # Get old list from Redis
    oldLinks = datastore.readRedis()
    # Get new list scraping hackaday
    links = scrape.extractVideoLinks()                 
    # Combine the lists (removing duplicates), dicts are not hashable so have to use for loop instead of sets
    links.extend(x for x in oldLinks if x not in links)
    print("Pushing %d posts." % len(links))
    # Push new list to Redis, limit size of list to 100 entries (trim from old end)
    datastore.updateRedis(links[:100])

sched.start()

